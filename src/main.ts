import { createApp } from 'vue'
import App from './App.vue'
import './assets/fonts.css'
import "@unocss/reset/tailwind.css";


createApp(App)
    .mount('#app')
